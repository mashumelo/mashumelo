from datetime import datetime
import speech_recognition as sr
for index, name in enumerate(sr.Microphone.list_microphone_names()):
    print("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))
import pyttsx3
import webbrowser
import wikipedia

#Speech engine initialization
engine = pyttsx3.init()
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[11].id) #0 = male, 1 = female
activationWord = 'computer' #Single word

#Configure browser
#Set the path
firefox_path = r"/usr/bin/firefox"
webbrowser.register('firefox', None, webbrowser.BackgroundBrowser(firefox_path))

#Configure Spotify
#Set the path
spotify_path = r"/usr/bin/spotify"
webbrowser.register('spotify', None, webbrowser.BackgroundBrowser(spotify_path))

#Configure Steam
#Set the path
steam_path = r"/home/mashumelo/.steam/debian-installation/steam.sh"
webbrowser.register('steam', None, webbrowser.BackgroundBrowser(steam_path))

#Configure Discord
#Set the path
discord_path = r"/usr/bin/discord"
webbrowser.register('discord', None, webbrowser.BackgroundBrowser(discord_path))

def speak(text, rate = 120):
    engine.setProperty('rate', rate)
    engine.say(text)
    engine.runAndWait()

def parseCommand():
    listener = sr.Recognizer()
    print('Listening for a command')

    with sr.Microphone() as source:
        listener.pause_threshhold = 2
        input_speech = listener.listen(source)

        try:
            print('Recognizing speech...')
            query = listener.recognize_google(input_speech, language='en_gb')
            print(f'The input speech was: {query}')
        except Exception as exception:
            print('I did not quite catch that')
            speak('I did not quite catch that')
            print(exception)
            return 'None'

        return query

def search_wikipedia(query = ' '):
    searchResults = wikipedia.search(query)
    if not searchResults:
        print('No wikipedia result')
        return 'No result received'
    try:
        wikiPage = wikipedia.page(searchResults[0])
    except wikipedia.DisambiguationError as error:
        wikiPage = wikipedia.page(error.options[0])
    print(wikiPage.title)
    wikiSummary = str(wikiPage.summary)
    print(' ')
    print(wikiSummary)
    return wikiSummary

def listOrDict(var):
    if isinstance(var, list):
        return var[0]['plaintext']
    else:
        return var['plaintext']




#Main loop
if __name__ == '__main__':
    speak('All systems nominal.', 120)

    while True:
        #Parse as a list
        query = parseCommand().lower().split()

        if query[0] == activationWord:
            query.pop(0)

            #List commands
            if query[0] == 'say':
                if 'hello' in query:
                    speak('Greetings, all.')
                else:
                    query.pop(0)
                    speech = ' '.join(query)
                    speak(speech)

            #Navigation
            if query[0] == 'go' and query[1] == 'to':
                speak('Opening...')
                query = ' '.join(query[2:])
                webbrowser.get('firefox').open_new(query)

            #Spotify Application
            if query[0] == 'open' and query[1] == 'spotify':
                speak('Opening...')
                query = ' '.join(query[1:])
                webbrowser.get('spotify').open_new(query)

            #Steam
            if query[0] == 'open' and query[1] == 'steam':
                speak('Opening...')
                query = ' '.join(query[1:])
                webbrowser.get('steam').open_new(query)

            #Discord
            if query[0] == 'open' and query[1] == 'discord':
                speak('Opening...')
                query = ' '.join(query[1:])
                webbrowser.get('discord').open_new(query)

            #wikipedia
            if query[0] == 'wikipedia':
                query = ' '.join(query[1:])
                speak('Querying the universal databank.')
                speak(search_wikipedia(query))

            #Note taking
            if query[0] == 'log':
                speak('Ready to record your note')
                newNote = parseCommand().lower()
                now = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
                with open('note_%.txt' % now, 'w') as newFile:
                    newFile.write(newNote)
                speak('Note written')

            if query[0] == 'exit':
                speak('Goodbye')
                break
