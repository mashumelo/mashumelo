# Bookmarks to important pages

Linux Mint Community Page
[Linux Mint Community Page](https://community.linuxmint.com/)

Fedora Documentation
[Fedora Start](https://start.fedoraproject.org/)

Arch Linux Wiki
[Arch Linux Wiki](https://wiki.archlinux.org/)

Arch User Repository
[AUR](https://aur.archlinux.org/)

Distrowatch
[Distrowatch Linux Distros](https://distrowatch.com/)

Clan/Team Discord Setup
[Clan/Team Discord Setup | Xenon Bot](https://xenon.bot/templates/bUVHUSU8RwqM)

UserBenchmark
[Home - UserBenchmark](https://www.userbenchmark.com/)

CC Search
[CC Search](https://search.creativecommons.org/)

PCPartPicker
[Pick parts. Build your PC. Compare and share. - PCPartPicker](https://pcpartpicker.com)

A complete collection of Discord emojis for Windows, Android, iOS and macOS
[List of Discord emojis. Copy and paste!](https://emojis.wiki/discord/)

VSCodium Github Page
[VSCodium Github Page](https://github.com/VSCodium/vscodium)